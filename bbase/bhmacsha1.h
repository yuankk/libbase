#ifndef __BASE_HMACSHA1_H__
#define __BASE_HMACSHA1_H__
#pragma once

#include <string>

namespace butils
{
    /*
     * @brief sha1 sha1算法
     * @param message 需要加密的数据
     * @param message_length 数据长度
     * @param digest 加密后数据
     * @return 
     */
    void sha1(unsigned char* message, int message_length, unsigned char* digest);

    /*
     * @brief hmacsha1 hmacsha1算法
     * @param key 需要加密的密钥
     * @param key_length 密钥长度
     * @param data 需要加密的数据
     * @param data_length 数据长度
     * @param digest 加密后数据
     * @return
     */
    void hmacsha1(unsigned char* key, int key_length, unsigned char* data, int data_length, unsigned char* digest);

    /*
     * @brief hmacsha1 hmacsha1算法
     * @param key 需要加密的密钥
     * @param data 需要加密的数据
     * @return 加密后数据
     */
    std::string hmacsha1(const std::string& key, const std::string& data);
}

#endif //__BASE_HMACSHA1_H__
