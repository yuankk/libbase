#include "blog.h"
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/daily_file_sink.h>

blog::~blog()
{
}

void blog::setup(const std::string& fname, int lv)
{
	auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
	auto daily_sink = std::make_shared<spdlog::sinks::daily_file_sink_mt>(fname, 0, 0);
	std::initializer_list<spdlog::sink_ptr> il{ console_sink, daily_sink };
	logger_ = std::make_shared<spdlog::logger>("logger_cdf", il);
	spdlog::register_logger(logger_);

	logger_->set_level(static_cast<spdlog::level::level_enum>(lv));
	logger_->set_pattern("[%Y-%m-%d %H:%M:%S %e] [%^%L%$] [T %t] %v");
	logger_->flush_on(spdlog::level::warn);
	spdlog::flush_every(std::chrono::seconds(3));
}

void blog::revoke()
{
	spdlog::shutdown();
}

void blog::set_level(int lv)
{
	if (logger_) logger_->set_level(static_cast<spdlog::level::level_enum>(lv));
}

int blog::level() const
{
	if (logger_)
		return logger_->level();
	else
		return 0;
}