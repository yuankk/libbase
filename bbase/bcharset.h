#ifndef __BASE_CHARSET_H__
#define __BASE_CHARSET_H__
#pragma once

#include <string>

namespace butils
{

class charset
{
public:
    /**
     * @brief bytes2hexs 内存中的字符转换为16进制，如内存字符串数据3aA-> 336141
     * @param data 需要转换的内容
     * @param len 内容长度
     * @return 转换后字符串
     */
    static std::string bytes2hexs(const char* data, size_t len);

    /**
     * @brief hexs2bytes 16进制字符转换为内存数据，如数据336141-> 3aA
     * @param data 需要转换的内容
     * @param len 内容长度
     * @return 转换后字符串
     */
    static std::string hexs2bytes(const char* data, size_t len);

    /**
     * @brief is_utf8 是否是UTF8编码
     * @param data 需要判断的内容
     * @param len 内容长度
     * @return 是utf8返回true
     */
    static bool is_utf8(const char* data, int len);

    /**
     * @brief is_utf8 是否是UTF8编码
     * @param str 需要判断的字符串
     * @return 是utf8返回true
     */
    static bool is_utf8(const std::string& str);

    /**
     * @brief ans_to_utf8 ANSI转UTF-8
     * @param a_str 需要转换的字符串
     * @return 转换后字符串
     */
    static const std::string ans_to_utf8(const std::string& a_str);

    /**
     * @brief utf8_to_ans UTF-8转ANSI
     * @param u_str 需要转换的字符串
     * @return 转换后字符串
     */
    static const std::string utf8_to_ans(const std::string& u_str);

    /**
     * @brief try_ans_to_utf8 尝试ANSI转UTF-8
     * @param a_str 需要转换的字符串
     * @return 转换后字符串
     */
    static const std::string try_ans_to_utf8(const std::string& a_str);
};

}

#endif //__BASE_CHARSET_H__
