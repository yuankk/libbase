#ifndef __BASE_BASE64_H__
#define __BASE_BASE64_H__
#pragma once

#include <string>

namespace butils
{

class base64
{
public:
    /** 
     * @brief encode base64加密
	 * @param input 需要加密的内容
	 * @param len 加密内容长度
	 * @return base64字符串
	 */
	static std::string encode(const unsigned char* input, size_t len);
    static std::string encode(const char* input, size_t len);

	/** 
     * @brief encode base64加密字符串
	 * @param input 需要加密的内容
	 * @return base64字符串
	 */
	static std::string encode(const std::string& input);

	/** 
     * @brief decode base64解密字符串
	 * @param input 需要解密的base64字符串
	 * @return 解密后的字符串
	 */
	static std::string decode(const std::string& input);

private:
	/** 
     * @brief is_base64 判断是否是有效base64字符
	 * @param c 需要判断的字符
	 * @return 如果c是有效base64字符，返回true；否则返回false
	 */
	static bool is_base64(unsigned char c);
};

}

#endif //__BASE_BASE64_H__
