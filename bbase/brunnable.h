#ifndef __BASE_RUNNABLE_H__
#define __BASE_RUNNABLE_H__

#include <queue>
#include <memory>
#include <mutex>
#include <thread>
#include <condition_variable>

/*
 *	brief 线程类，封装了基本的线程操作，
 *		  消息传递的机制
 */
class brunnable
{
public:
	brunnable() = default;
	virtual ~brunnable();

	// 线程操作
	virtual void start();
	virtual	void stop();

	// 消息投递
	virtual void push(std::shared_ptr<void> data);
	virtual std::shared_ptr<void> pop();
	
	// 消息处理
	virtual void run();
	virtual void handler(std::shared_ptr<void> data) = 0;

private:
	// 线程函数
	void thread_func();

protected:
	bool								stop_{ false };
	std::condition_variable				thread_cv_;
	std::unique_ptr<std::thread>		thread_;
	std::mutex							mtx_;
	std::queue<std::shared_ptr<void>>	queue_;
};

#endif // __RUNNABLE_H__