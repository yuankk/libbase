#ifndef __BASE_STRINGS_H__
#define __BASE_STRINGS_H__
#pragma once

#include <string>
#include <vector>

namespace butils {

class strings
{

public:
    /**
     * @brief split 拆分字符串
	 * @param src 需要拆分的字符串
	 * @param sep 分隔字符
	 * @return 拆分后的字符串
	 */
    static std::vector<std::string> split(std::string src, char sep);

    /** 
     * @brief split 用分隔符切割字符串()
     * @param str 输入字符串
     * @param left_str 分隔后得到的左字符串
     * @param right_str 分隔后得到的右字符串（不包含sep）
     * @param sep 分隔符
     * @return 分隔成与否
     */
    static bool split(const std::string &str, const std::string &sep, std::string *pleft, std::string *pright);

    /**
     * @brief compare_ingore_case 忽略大小写比较字符串 
     * @param str1 需要比较的字符串1
     * @param str2 需要比较的字符串2
     * @return 
     */
    static bool compare_ingore_case(const std::string &str1, const std::string &str2);
    
    /**
     * @brief start_with 是否以prefix开头, 大小写敏感
     * @param str 字符串
     * @param prefix 前缀
     * @return 如果以开头，则返回true
     */
    static bool start_with(const std::string &str, const std::string &prefix);

    /**
     * @brief end_with 是否以suffix结尾, 大小写敏感
     * @param str 字符串
     * @param suffix 后缀
     * @return 如果以结尾，则返回true
     */
    static bool end_with(const std::string &str, const std::string &suffix);

    /**
     * @brief trim 去掉字符串前面和后面的空格符,Tab符等空白符
     * @param str 字符串
     */
    static void trim(std::string& str);

    /**
     * @brief strip 去掉字符串前面和后面的空格符,Tab符等空白符
     * @param str 字符串
     * @return 返回处理后的字符串
     */
    static std::string strip(const std::string& str);
};

}

#endif //__BASE_STRINGS_H__
