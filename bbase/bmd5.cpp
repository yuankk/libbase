#include "bmd5.h"

#define UUID_DETAIL_MD5_STEP(f, a, b, c, d, x, t, s) \
        (a) += f((b), (c), (d)) + (x) + (t); \
        (a) = (((a) << (s)) | (((a) & 0xffffffff) >> (32 - (s)))); \
        (a) += (b);

#if defined(__i386__) || defined(__x86_64__) || defined(__vax__)
#define UUID_DETAIL_MD5_SET(n) \
        (*(MD5_u32plus *)&ptr[(n) * 4])
#define UUID_DETAIL_MD5_GET(n) \
        UUID_DETAIL_MD5_SET(n)
#else
#define UUID_DETAIL_MD5_SET(n) \
        (ctx->block[(n)] = \
        (MD5_u32plus)ptr[(n) * 4] | \
        ((MD5_u32plus)ptr[(n) * 4 + 1] << 8) | \
        ((MD5_u32plus)ptr[(n) * 4 + 2] << 16) | \
        ((MD5_u32plus)ptr[(n) * 4 + 3] << 24))
#define UUID_DETAIL_MD5_GET(n) \
        (ctx->block[(n)])
#endif

#define UUID_DETAIL_MD5_OUT(dst, src) \
        (dst)[0] = (unsigned char)(src); \
        (dst)[1] = (unsigned char)((src) >> 8); \
        (dst)[2] = (unsigned char)((src) >> 16); \
        (dst)[3] = (unsigned char)((src) >> 24);

butils::md5::md5()
{
	MD5_Init(&ctx_);
}

butils::md5::md5(const std::string& message) : md5()
{
	MD5_Update(&ctx_, (const void*)message.data(), static_cast<unsigned long>(message.length()));
}

butils::md5::md5(const char* message) : md5()
{
	MD5_Update(&ctx_, (const void*)message, static_cast<unsigned long>(std::strlen(message)));
}

butils::md5::md5(const void* message, std::size_t size) : md5()
{
	MD5_Update(&ctx_, message, static_cast<unsigned long>(size));
}

std::string butils::md5::str(bool upper)
{
	char hex_upper[16] = {
		'0', '1', '2', '3',
		'4', '5', '6', '7',
		'8', '9', 'A', 'B',
		'C', 'D', 'E', 'F'
	};
	char hex_lower[16] = {
		'0', '1', '2', '3',
		'4', '5', '6', '7',
		'8', '9', 'a', 'b',
		'c', 'd', 'e', 'f'
	};

	std::uint8_t digest[16]{ 0 };
	get_digest(digest);
	std::string str;
	str.reserve(16 << 1);
	for (std::size_t i = 0; i < 16; ++i)
	{
		int t = digest[i];
		int a = t / 16;
		int b = t % 16;
		str.append(1, upper ? hex_upper[a] : hex_lower[a]);
		str.append(1, upper ? hex_upper[b] : hex_lower[b]);
	}
	return str;
}

void butils::md5::process_byte(unsigned char byte)
{
	MD5_Update(&ctx_, &byte, 1);
}

void butils::md5::process_bytes(void const* buffer, std::size_t byte_count)
{
	MD5_Update(&ctx_, buffer, static_cast<unsigned long>(byte_count));
}

void butils::md5::get_digest(std::uint8_t digest[16])
{
	MD5_Final(reinterpret_cast<unsigned char *>(&digest[0]), &ctx_);
}

const void* butils::md5::body(MD5_CTX* ctx, const void* data, unsigned long size)
{
	const unsigned char *ptr;
	MD5_u32plus a, b, c, d;
	MD5_u32plus saved_a, saved_b, saved_c, saved_d;

	ptr = (const unsigned char *)data;

	a = ctx->a;
	b = ctx->b;
	c = ctx->c;
	d = ctx->d;

	do {
		saved_a = a;
		saved_b = b;
		saved_c = c;
		saved_d = d;

		/* Round 1 */
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, a, b, c, d, UUID_DETAIL_MD5_SET(0), 0xd76aa478, 7)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, d, a, b, c, UUID_DETAIL_MD5_SET(1), 0xe8c7b756, 12)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, c, d, a, b, UUID_DETAIL_MD5_SET(2), 0x242070db, 17)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, b, c, d, a, UUID_DETAIL_MD5_SET(3), 0xc1bdceee, 22)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, a, b, c, d, UUID_DETAIL_MD5_SET(4), 0xf57c0faf, 7)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, d, a, b, c, UUID_DETAIL_MD5_SET(5), 0x4787c62a, 12)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, c, d, a, b, UUID_DETAIL_MD5_SET(6), 0xa8304613, 17)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, b, c, d, a, UUID_DETAIL_MD5_SET(7), 0xfd469501, 22)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, a, b, c, d, UUID_DETAIL_MD5_SET(8), 0x698098d8, 7)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, d, a, b, c, UUID_DETAIL_MD5_SET(9), 0x8b44f7af, 12)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, c, d, a, b, UUID_DETAIL_MD5_SET(10), 0xffff5bb1, 17)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, b, c, d, a, UUID_DETAIL_MD5_SET(11), 0x895cd7be, 22)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, a, b, c, d, UUID_DETAIL_MD5_SET(12), 0x6b901122, 7)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, d, a, b, c, UUID_DETAIL_MD5_SET(13), 0xfd987193, 12)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, c, d, a, b, UUID_DETAIL_MD5_SET(14), 0xa679438e, 17)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_F, b, c, d, a, UUID_DETAIL_MD5_SET(15), 0x49b40821, 22)

		/* Round 2 */
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, a, b, c, d, UUID_DETAIL_MD5_GET(1), 0xf61e2562, 5)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, d, a, b, c, UUID_DETAIL_MD5_GET(6), 0xc040b340, 9)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, c, d, a, b, UUID_DETAIL_MD5_GET(11), 0x265e5a51, 14)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, b, c, d, a, UUID_DETAIL_MD5_GET(0), 0xe9b6c7aa, 20)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, a, b, c, d, UUID_DETAIL_MD5_GET(5), 0xd62f105d, 5)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, d, a, b, c, UUID_DETAIL_MD5_GET(10), 0x02441453, 9)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, c, d, a, b, UUID_DETAIL_MD5_GET(15), 0xd8a1e681, 14)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, b, c, d, a, UUID_DETAIL_MD5_GET(4), 0xe7d3fbc8, 20)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, a, b, c, d, UUID_DETAIL_MD5_GET(9), 0x21e1cde6, 5)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, d, a, b, c, UUID_DETAIL_MD5_GET(14), 0xc33707d6, 9)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, c, d, a, b, UUID_DETAIL_MD5_GET(3), 0xf4d50d87, 14)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, b, c, d, a, UUID_DETAIL_MD5_GET(8), 0x455a14ed, 20)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, a, b, c, d, UUID_DETAIL_MD5_GET(13), 0xa9e3e905, 5)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, d, a, b, c, UUID_DETAIL_MD5_GET(2), 0xfcefa3f8, 9)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, c, d, a, b, UUID_DETAIL_MD5_GET(7), 0x676f02d9, 14)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_G, b, c, d, a, UUID_DETAIL_MD5_GET(12), 0x8d2a4c8a, 20)

		/* Round 3 */
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H, a, b, c, d, UUID_DETAIL_MD5_GET(5), 0xfffa3942, 4)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H2, d, a, b, c, UUID_DETAIL_MD5_GET(8), 0x8771f681, 11)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H, c, d, a, b, UUID_DETAIL_MD5_GET(11), 0x6d9d6122, 16)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H2, b, c, d, a, UUID_DETAIL_MD5_GET(14), 0xfde5380c, 23)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H, a, b, c, d, UUID_DETAIL_MD5_GET(1), 0xa4beea44, 4)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H2, d, a, b, c, UUID_DETAIL_MD5_GET(4), 0x4bdecfa9, 11)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H, c, d, a, b, UUID_DETAIL_MD5_GET(7), 0xf6bb4b60, 16)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H2, b, c, d, a, UUID_DETAIL_MD5_GET(10), 0xbebfbc70, 23)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H, a, b, c, d, UUID_DETAIL_MD5_GET(13), 0x289b7ec6, 4)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H2, d, a, b, c, UUID_DETAIL_MD5_GET(0), 0xeaa127fa, 11)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H, c, d, a, b, UUID_DETAIL_MD5_GET(3), 0xd4ef3085, 16)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H2, b, c, d, a, UUID_DETAIL_MD5_GET(6), 0x04881d05, 23)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H, a, b, c, d, UUID_DETAIL_MD5_GET(9), 0xd9d4d039, 4)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H2, d, a, b, c, UUID_DETAIL_MD5_GET(12), 0xe6db99e5, 11)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H, c, d, a, b, UUID_DETAIL_MD5_GET(15), 0x1fa27cf8, 16)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_H2, b, c, d, a, UUID_DETAIL_MD5_GET(2), 0xc4ac5665, 23)

		/* Round 4 */
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, a, b, c, d, UUID_DETAIL_MD5_GET(0), 0xf4292244, 6)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, d, a, b, c, UUID_DETAIL_MD5_GET(7), 0x432aff97, 10)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, c, d, a, b, UUID_DETAIL_MD5_GET(14), 0xab9423a7, 15)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, b, c, d, a, UUID_DETAIL_MD5_GET(5), 0xfc93a039, 21)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, a, b, c, d, UUID_DETAIL_MD5_GET(12), 0x655b59c3, 6)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, d, a, b, c, UUID_DETAIL_MD5_GET(3), 0x8f0ccc92, 10)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, c, d, a, b, UUID_DETAIL_MD5_GET(10), 0xffeff47d, 15)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, b, c, d, a, UUID_DETAIL_MD5_GET(1), 0x85845dd1, 21)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, a, b, c, d, UUID_DETAIL_MD5_GET(8), 0x6fa87e4f, 6)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, d, a, b, c, UUID_DETAIL_MD5_GET(15), 0xfe2ce6e0, 10)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, c, d, a, b, UUID_DETAIL_MD5_GET(6), 0xa3014314, 15)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, b, c, d, a, UUID_DETAIL_MD5_GET(13), 0x4e0811a1, 21)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, a, b, c, d, UUID_DETAIL_MD5_GET(4), 0xf7537e82, 6)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, d, a, b, c, UUID_DETAIL_MD5_GET(11), 0xbd3af235, 10)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, c, d, a, b, UUID_DETAIL_MD5_GET(2), 0x2ad7d2bb, 15)
		UUID_DETAIL_MD5_STEP(UUID_DETAIL_MD5_I, b, c, d, a, UUID_DETAIL_MD5_GET(9), 0xeb86d391, 21)

		a += saved_a;
		b += saved_b;
		c += saved_c;
		d += saved_d;

		ptr += 64;
	} while (size -= 64);

	ctx->a = a;
	ctx->b = b;
	ctx->c = c;
	ctx->d = d;

	return ptr;
}

void butils::md5::MD5_Init(MD5_CTX* ctx)
{
	ctx->a = 0x67452301;
	ctx->b = 0xefcdab89;
	ctx->c = 0x98badcfe;
	ctx->d = 0x10325476;

	ctx->lo = 0;
	ctx->hi = 0;
}

void butils::md5::MD5_Update(MD5_CTX* ctx, const void* data, unsigned long size)
{
	MD5_u32plus saved_lo;
	unsigned long used, available;

	saved_lo = ctx->lo;
	if ((ctx->lo = (saved_lo + size) & 0x1fffffff) < saved_lo)
		ctx->hi++;
	ctx->hi += size >> 29;

	used = saved_lo & 0x3f;

	if (used) {
		available = 64 - used;

		if (size < available) {
			memcpy(&ctx->buffer[used], data, size);
			return;
		}

		memcpy(&ctx->buffer[used], data, available);
		data = (const unsigned char *)data + available;
		size -= available;
		body(ctx, ctx->buffer, 64);
	}

	if (size >= 64) {
		data = body(ctx, data, size & ~(unsigned long)0x3f);
		size &= 0x3f;
	}

	memcpy(ctx->buffer, data, size);
}

void butils::md5::MD5_Final(unsigned char* result, MD5_CTX* ctx)
{
	unsigned long used, available;

	used = ctx->lo & 0x3f;

	ctx->buffer[used++] = 0x80;

	available = 64 - used;

	if (available < 8) {
		memset(&ctx->buffer[used], 0, available);
		body(ctx, ctx->buffer, 64);
		used = 0;
		available = 64;
	}

	memset(&ctx->buffer[used], 0, available - 8);

	ctx->lo <<= 3;
	UUID_DETAIL_MD5_OUT(&ctx->buffer[56], ctx->lo)
	UUID_DETAIL_MD5_OUT(&ctx->buffer[60], ctx->hi)
	body(ctx, ctx->buffer, 64);

	UUID_DETAIL_MD5_OUT(&result[0], ctx->a)
	UUID_DETAIL_MD5_OUT(&result[4], ctx->b)
	UUID_DETAIL_MD5_OUT(&result[8], ctx->c)
	UUID_DETAIL_MD5_OUT(&result[12], ctx->d)
	memset(ctx, 0, sizeof(*ctx));
}
