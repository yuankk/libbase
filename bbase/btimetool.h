#ifndef __BASE_BTIME_H__
#define __BASE_BTIME_H__
#pragma once

#include <string>

namespace butils
{

class timetool
{
public:
    /*
     * @brief timestamps 时间戳(秒)
     * @return 当前时间戳(秒)
     */
    static time_t timestamps();

    /*
     * @brief timestampms 时间戳(毫秒)
     * @return 当前时间戳(毫秒)
     */
    static time_t timestampms();

    /*
     * @brief unix_to_sdate 时间戳转日期 %Y-%m-%d
     * @param ts 转换的时间戳
     * @return 转换后的日期
     */
    static std::string unix_to_sdate(time_t ts);

    /*
     * @brief unix_to_stime 时间戳转日期 %Y-%m-%d %H:%M:%S
     * @param ts 转换的时间戳
     * @return 转换后的时间
     */
    static std::string unix_to_stime(time_t ts);

    /*
     * @brief stime_to_unix 时间转Unix时间戳 %Y-%m-%d %H:%M:%S
     * @param stime 转换的时间
     * @return 转换后的时间戳
     */
    static time_t stime_to_unix(const std::string& stime);
};

}
#endif //__BASE_BTIME_H__
