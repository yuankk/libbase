#ifndef __BASE_VERIFY_H__
#define __BASE_VERIFY_H__
#pragma once

namespace butils
{

class verify
{
public:
    /**
     * @brief crc16 crc16位校验码
     * @param q 需要计算内容
     * @param len 内容长度
     * @return 返回校验码
     */
    static unsigned short crc16(unsigned char *q, size_t len);

    /*
     * @brief int_3bytes 3字节转int
     * @param c0 第一个byte值
     * @param c1 第二个byte值
     * @param c2 第三个byte值
     * @return int值
     */
    static int int_3bytes(char c0, char c1, char c2);
};

}

#endif //__BASE_VERIFY_H__