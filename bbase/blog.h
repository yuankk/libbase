#ifndef __BASE_LOG_H__
#define __BASE_LOG_H__
#pragma once

#include "bsingleton.h"
#include <spdlog/spdlog.h>

class blog : public bsingleton<blog>
{
private:
	std::shared_ptr<spdlog::logger> logger_;

public:
	virtual ~blog();
	//
	void setup(const std::string& fname = "logs/daily.log", int lv = 0);
	void revoke();
	//
	void set_level(int lv = 0);
	// 
	int level() const;

	//
	template<typename... Args>
	inline void trace(fmt::format_string<Args...> fmt, Args &&...args)
	{
		if (logger_) logger_->trace(fmt, std::forward<Args>(args)...);
	}
	template<typename... Args>
	inline void debug(fmt::format_string<Args...> fmt, Args &&...args)
	{
		if (logger_) logger_->debug(fmt, std::forward<Args>(args)...);
	}
	template<typename... Args>
	inline void info(fmt::format_string<Args...> fmt, Args &&...args)
	{
		if (logger_) logger_->info(fmt, std::forward<Args>(args)...);
	}
	template<typename... Args>
	inline void warn(fmt::format_string<Args...> fmt, Args &&...args)
	{
		if (logger_) logger_->warn(fmt, std::forward<Args>(args)...);
	}
	template<typename... Args>
	inline void error(fmt::format_string<Args...> fmt, Args &&...args)
	{
		if (logger_) logger_->error(fmt, std::forward<Args>(args)...);
	}
	template<typename... Args>
	inline void critical(fmt::format_string<Args...> fmt, Args &&...args)
	{
		if (logger_) logger_->critical(fmt, std::forward<Args>(args)...);
	}
};

#define gLog blog::instance()

#define logt(...) gLog->trace(__VA_ARGS__)
#define logd(...) gLog->debug(__VA_ARGS__)
#define logm(...) gLog->info(__VA_ARGS__)
#define logw(...) gLog->warn(__VA_ARGS__)
#define loge(...) gLog->error(__VA_ARGS__)
#define logf(...) gLog->critical(__VA_ARGS__)

#endif	// __LOG_H__
