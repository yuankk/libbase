#ifndef __BASE_UTILS_H__
#define __BASE_UTILS_H__

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <string>

// 大小端转换
#ifndef kSwap16
#define kSwap16(x) \
    ((unsigned short)( \
        (((unsigned short)(x) & (unsigned short)0x00ffU) << 8 ) | \
        (((unsigned short)(x) & (unsigned short)0xff00U) >> 8 ) ))
#endif //kSwap16

#ifndef kSwap32
#define kSwap32(x) \
    ((unsigned)( \
        (((unsigned)(x) & 0x000000FFU) << 24) | \
		(((unsigned)(x) & 0x0000FF00U) << 8 ) | \
        (((unsigned)(x) & 0x00FF0000U) >> 8 ) | \
		(((unsigned)(x) & 0xFF000000U) >> 24) ))
#endif //kSwap32

namespace butils
{
#ifdef _WIN32
    /*
     * @brief current_module_dir 当前模块路径
     * @return 当前模块路径
     */
    std::string current_module_dir();
#else
	int open_dev_null(int fd);
	void daemonize(char *dir);
	void set_corefile_unlimit();
	void set_sockfile_size(size_t size);
	void set_processstack_size(size_t size);
	int write_process_pid(char *dir);
#endif // _WIN32

	/*
	 * 16位大小端转换
	 * @param x 转换的值
	 * @return 转换后的值
	 */
	unsigned short swap16(unsigned short x);

	/*
	 * 32位大小端转换
	 * @param x 转换的值
	 * @return 转换后的值
	 */
	unsigned int swap32(unsigned int x);
	
	/*
	 * 创建文件夹
	 * @param path 文件夹路径
	 * @return 创建结果
	*/
	int create_folder(const std::string& path);

    /*
     * @brief worker_dir 当前工作路径
     * @return 当前工作路径
     */
    std::string worker_dir();

	/*
	 * 异或加解密
	 * @param data 加密的值
	 * @param key 加密的Key
	 */
	void encrypt_decrypt_xor(std::string& data, const char* key);

    /*
     * @brief uuid UUID生成
     * @return 生成的UUID
     */
    std::string uuid();
};

#endif //__BASE_UTILS_H__