#ifndef __BASE_SINGLETON_H__
#define __BASE_SINGLETON_H__
#pragma once

/*
 *	brief 单例模板类
 * 在 C++ 11 之后，被 static 修饰的变量可以保证是线程安全的；
 * 通过禁用单例类的 copy constructor，move constructor 和 operator= 可以防止类的唯一实例被拷贝或移动；
 * 不暴露单例类的 constructor 和 destructor 可以保证单例类不会通过其他途径被实例化，同时将两者定义为 protected 可以让其被子类继承并使用。
 */
template<typename T>
class bsingleton
{
public:
	static T* instance()
	{
		static T t;
		return &t;
	}

	bsingleton(const bsingleton&) = delete;
	bsingleton& operator=(const bsingleton&) = delete;
	bsingleton(const bsingleton&&) = delete;
	bsingleton& operator=(const bsingleton&&) = delete;

protected:
	explicit bsingleton() = default;
	virtual ~bsingleton() = default;
};

#endif //__SINGLETON_H__
