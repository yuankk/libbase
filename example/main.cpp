#include <iostream>
#include <string>
#include <bbase/blog.h>
#include <bbase/binifile.h>
#include <bbase/butils.h>

#ifdef _WIN32
#include <bbase/bdump.h>
#endif

int main(int argc, char* argv[])
{
#ifdef _WIN32
    // 设置控制台编码为UTF-8
    SetConsoleOutputCP(CP_UTF8);
    // 捕获dump
    SetUnhandledExceptionFilter(UnhandledExceptionFilterEx);
    // 切换工作路径
    _chdir(butils::current_module_dir().c_str());
#endif
    // 初始化日志文件
    gLog->setup("logs/srv.log");
    gLog->set_level(1);
    // INI 文件读取
    logm("启动，工作路径=> {}", butils::worker_dir());
    butils::ini::file iif;
    if (0 != iif.load("config.ini"))
    {
        logw("配置文件读取错误: {}", iif.last_err_msg());
        return false;
    }
    // 打印配置文件
    iif.print();
    std::string val = iif.string_value("server", "this_ip", "192.168.4.246");
    // 修改并保存配置文件
    iif.set_value("server", "this_ip", "192.168.3.17", " 当前服务IP地址");
    iif.save();

    // 
    logm("生成UUID: {}", butils::uuid());

    gLog->revoke();
    return 0;
}
